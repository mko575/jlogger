---
menu:
  after:
    name: blog
    weight: 5
title: Blog
---

This documentation may one day receive some blog posts ... but for now they shouldn't be any. I just keep it as a placeholder
.