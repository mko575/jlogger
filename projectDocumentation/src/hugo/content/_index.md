---
title: Introduction
type: docs
---

# JLogger

This website contains JLogger's documentation. It is for now minimal, with the main objective to deploy JLogger's generated Javadoc.

It will hopefully one day contain more information. For now, you can have a look at the [HelloWorld](https://gitlab.com/mko575/jlogger/-/blob/main/subprojects/HelloWorld/src/main/java/com/gitlab/mko575/jlogger/helloworld/HelloWorld.java) to get a feel of JLogger's usage.