#!/bin/bash

useSameName="no";
compileSeparatly="yes";

srcDir="./core/src"

buildDir="./build";
modClassesDir="${buildDir}/mods";
modLibDir="${buildDir}/mlibs";

libModules="com.gitlab.mko.jlogger#JLogger"
mainModule="com.gitlab.mko.jlogger.helloworld#HelloWorld"
mainClass="com.gitlab.mko575.jlogger.helloworld.HelloWorld"

modules="${libModules} ${mainModule}";

if [ "${useSameName}" == "yes" ];
then
    modules="com.gitlab.mko.jlogger#com.gitlab.mko.jlogger com.gitlab.mko.jlogger.helloworld#com.gitlab.mko.jlogger.helloworld";
fi;

# Compiling modules

rm -fr ${modClassesDir};
mkdir --parents ${modClassesDir};

PP_TITLE="\033[1;96m";
PP_INFO="\033[0;36m";
PP_CMD="\033[1;32m";
PP_END="\033[0m"

if [ "${compileSeparatly}" == "yes" ];
then

    for m in ${modules};
    do
        moduleName=$(echo ${m} | cut -d '#' -f 1);
        moduleSrcPath=$(echo ${m} | cut -d '#' -f 2);
        
        echo -e "\n${PP_TITLE}### Compiling module ${moduleName} from ${moduleSrcPath}! ###${PP_END}\n";
        
        outputDir="${modClassesDir}/${moduleName}";
        mkdir --parents ${outputDir};
        
        cmd="javac -Xlint:unchecked --module-path ${modClassesDir} -d ${outputDir} ${srcDir}/${moduleSrcPath}/java/module-info.java $(find ${srcDir}/${moduleSrcPath}/java -name '*.java' -printf " %p")";
        echo -e "${PP_INFO}I will execute:${PP_END} ${PP_CMD}${cmd}${PP_END}\n";
        ${cmd};
    done;

else
    echo -e "\n${PP_TITLE}### Compiling modules $(echo ${modules} | cut -d '#' -f 1) all at once! ###${PP_END}\n";
        
    cmd="javac -Xlint:unchecked -d ${modClassesDir} --module-source-path ${srcDir} $(find ${srcDir} -name "*.java")";
    echo -e "${PP_INFO}I will execute:${PP_END} ${PP_CMD}${cmd}${PP_END}\n";
    ${cmd};
fi;

echo -e "\n${PP_TITLE}### Executing the main module from class files! ###${PP_END}\n";

cmd="java --module-path ${modClassesDir} -m $(echo ${mainModule} | cut -d '#' -f 1)/${mainClass}"
echo -e "${PP_INFO}I will execute:${PP_END} ${PP_CMD}${cmd}${PP_END}\n";
${cmd};

# Packaging modules

rm -fr ${modLibDir};
mkdir --parents ${modLibDir};

for m in ${libModules};
do
    moduleName=$(echo ${m} | cut -d '#' -f 1);
        
    echo -e "\n${PP_TITLE}### Packaging module ${moduleName}! ###${PP_END}\n";
        
    cmd="jar --create --file=${modLibDir}/${moduleName}@1.0.jar --module-version=1.0 -C ${modClassesDir}/${moduleName} .";
    echo -e "${PP_INFO}I will execute:${PP_END} ${PP_CMD}${cmd}${PP_END}\n";
    ${cmd};
done;

mainModuleName="$(echo ${mainModule} | cut -d '#' -f 1)";

echo -e "\n${PP_TITLE}### Packaging main module ${mainModuleName}! ###${PP_END}\n";

cmd="jar --create --file=${modLibDir}/${mainModuleName}.jar --main-class=${mainClass} -C ${modClassesDir}/${mainModuleName} ."
echo -e "${PP_INFO}I will execute:${PP_END} ${PP_CMD}${cmd}${PP_END}\n";
${cmd};

echo -e "\n${PP_TITLE}### Executing the main module from packages! ###${PP_END}\n";

cmd="java -p ${modLibDir} -m ${mainModuleName}"
echo -e "${PP_INFO}I will execute:${PP_END} ${PP_CMD}${cmd}${PP_END}\n";
${cmd};
