package com.gitlab.mko575.jlogger.subscribers.providers.defaultfile;

import com.gitlab.mko575.jlogger.JLogger;
import com.gitlab.mko575.jlogger.subscribers.FileSubscriber;
import com.gitlab.mko575.jlogger.subscribers.ISubscriberProvider;
import java.io.IOException;
import java.util.Optional;
import java.util.ServiceLoader;

/**
 * This utility class provides a default file subscriber for logs handled by JLogger.
 * This subscriber is automatically loaded by {@link JLogger#start()} using
 * <code>ServiceLoader.load(ISubscriberProvider.class)</code> and the
 * {@link DefaultFileSubscriberProvider#provider()} method.
 *
 * The provided subscriber has the name {@value #SUBSCRIBER_NAME}, defined in
 * {@link DefaultFileSubscriberProvider#SUBSCRIBER_NAME}.
 *
 * @see ServiceLoader#load(Class)
 * @see ISubscriberProvider
 */
public final class DefaultFileSubscriberProvider
    implements ISubscriberProvider<FileSubscriber> {

  /**
   * Lone instance of this class.
   */
  private static final DefaultFileSubscriberProvider LONE_INSTANCE =
      new DefaultFileSubscriberProvider();

  /**
   * The subscriber provided by this provider.
   */
  private Optional<FileSubscriber> subscriber;

  /**
   * Name of the subscriber provided: {@value #SUBSCRIBER_NAME}
   */
  public static final String SUBSCRIBER_NAME = "JLogger.DefaultFileSubscriber";

  /**
   * Private constructor preventing others to create new instances of this class.
   */
  private DefaultFileSubscriberProvider() { super(); }

  /**
   * Returns the sole instance of this class.
   * @return the sole instance of this class.
   */
  public static DefaultFileSubscriberProvider provider() {
    return LONE_INSTANCE;
  }

  @Override
  public Optional<FileSubscriber> getSubscriber() {
    if (subscriber == null) {
      try {
        subscriber = Optional.of(
            new FileSubscriber(
                SUBSCRIBER_NAME,
                null)
        );
      } catch (IOException e) {
        System.out.printf(
            "Error while initializing the default file subscriber: %s%n",
            e.getMessage()
        );
        subscriber = Optional.empty();
      }
    }
    return subscriber;
  }
}
