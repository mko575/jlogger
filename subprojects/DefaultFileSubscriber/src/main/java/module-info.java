import com.gitlab.mko575.jlogger.subscribers.ISubscriberProvider;
import com.gitlab.mko575.jlogger.subscribers.providers.defaultfile.DefaultFileSubscriberProvider;

module com.gitlab.mko.jlogger.subscribers.providers.defaultfile {
  requires transitive com.gitlab.mko.jlogger.core;

  exports com.gitlab.mko575.jlogger.subscribers.providers.defaultfile;

  provides ISubscriberProvider with DefaultFileSubscriberProvider;
}
