package com.gitlab.mko575.jlogger.helloworld;

import java.io.Serial;
import java.util.Arrays;
import java.util.Objects;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public abstract class ErrorMessages {

  // public static final CLIOutput mainAppOutput = CLIOutput.getCLIOutput("MainForestryOutput");
  public static final Logger mainAppLogger = Logger.getLogger("HelloWorld");
  private static final Level INITIAL_LOGGING_LEVEL = Level.INFO;

  /**
   * https://docs.oracle.com/cd/E17277_02/html/GettingStartedGuide/managelogging.html
   * https://www.logicbig.com/tutorials/core-java-tutorial/logging/levels.html
   * https://stackoverflow.com/questions/6307648/change-global-setting-for-logger-instances
   */
  {
    final ConsoleHandler mainLoggerCH = new ConsoleHandler();
    mainAppLogger.addHandler(mainLoggerCH);
  }

  /**
   * Sets the logging level globally.
   * @param logLevel the desired logging level.
   */
  private static void setGlobalLoggingLevel(Level logLevel) {
    Logger rootLogger = LogManager.getLogManager().getLogger("");
    rootLogger.setLevel(logLevel);
    for (Handler h : rootLogger.getHandlers()) {
      h.setLevel(logLevel);
    }
    mainAppLogger.setLevel(logLevel);
    Arrays.stream(mainAppLogger.getHandlers()).forEach(h -> h.setLevel(logLevel));
  }

  /**
   * Either or not to retrieve the stacktrace for implementation errors. Hard coded
   * value is {@value #LOG_IMPLEMENTATION_ERROR_WITH_STACKTRACE}.
   */
  private static final boolean LOG_IMPLEMENTATION_ERROR_WITH_STACKTRACE = true;

  private static final String IMPLEMENTATION_ERROR_PREFIX =
      "\n===== This should never have been thrown! =====\n";

  public enum ErrorMessagesId {
    IMPLEMENTATION_ERROR_IN_S_OF_S_S, IMPLEMENTATION_ERROR_IN_S_OF_S_AT_S_S,
    CASE_IMPLEMENTATION_MISSING, CASE_IMPLEMENTATION_MISSING_IN_S_OF_S, CASE_IMPLEMENTATION_MISSING_IN_S_OF_S_FOR_S_AT_P,
    TRYING_TO_AGGREGATE_INCOMPATIBLE_DATA_S_AND_S,
    FILE_S_CAN_NOT_BE_FOUND_OR_READ,
    SYNTAX_ERROR_AT_P, SYNTAX_ERROR_AT_L_S,
    ONTOLOGY_S_NOT_LOADED_AT_P,
    ONTOLOGY_S_DOES_NOT_DEFINE_S, ONTOLOGY_S_DOES_NOT_DEFINE_S_ERROR_AT_L
  }

  public static String get(ErrorMessagesId id) {
    switch (id) {
      case IMPLEMENTATION_ERROR_IN_S_OF_S_S:
        return IMPLEMENTATION_ERROR_PREFIX
            + " There is an implementation error in method %s of class %s: %s!";
      case IMPLEMENTATION_ERROR_IN_S_OF_S_AT_S_S:
        return IMPLEMENTATION_ERROR_PREFIX
            + " There is an implementation error in method %s of class %s at line %d: %s!";
      case CASE_IMPLEMENTATION_MISSING:
        return "There is a case missing.";
      case CASE_IMPLEMENTATION_MISSING_IN_S_OF_S:
        return IMPLEMENTATION_ERROR_PREFIX
            + " There is a case missing in method %s of class %s.";
      case CASE_IMPLEMENTATION_MISSING_IN_S_OF_S_FOR_S_AT_P:
        return IMPLEMENTATION_ERROR_PREFIX
            + " There is a case missing in method %s of class %s for SOSL file '%s' at (%d:%d).";
      case TRYING_TO_AGGREGATE_INCOMPATIBLE_DATA_S_AND_S:
        return IMPLEMENTATION_ERROR_PREFIX
            + " Trying to aggregate incompatible data in method %s of class %s: '%s' and '%s'.";
      case FILE_S_CAN_NOT_BE_FOUND_OR_READ:
        return "File '%s' can not be found or read.";
      case SYNTAX_ERROR_AT_P:
        return "Syntax error at %d:%d. See parser error messages.";
      case SYNTAX_ERROR_AT_L_S:
        return "Syntax error in %s at (%d:%d): %s.";
      case ONTOLOGY_S_NOT_LOADED_AT_P:
        return "Ontology '%s' has not been loaded yet at %d:%d.";
      case ONTOLOGY_S_DOES_NOT_DEFINE_S:
        return "Ontology '%s' does not define '%s'.";
      case ONTOLOGY_S_DOES_NOT_DEFINE_S_ERROR_AT_L:
        return "Ontology '%s' does not define '%s' (error in %s at %d:%d).";
    }
    return "Missing error message";
  }

  static class Debug extends Level {
    @Serial
    private static final long serialVersionUID = 1L;
    private Class<?> debuggedClass;
    protected Debug(Class<?> c) {
      super("DEBUG", 100);
      debuggedClass = c;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      if (!super.equals(o)) {
        return false;
      }
      Debug debug = (Debug) o;
      return Objects.equals(debuggedClass, debug.debuggedClass);
    }

    @Override
    public int hashCode() {
      return Objects.hash(super.hashCode(), debuggedClass);
    }
  }

  public static Level debugLevelFor(Class<Object> c) {
    return new Debug(c);
  }

  public static void signal(Level lvl, boolean includingOutput, String errorMsg) {
    /*
      if ( includingOutput )
        mainAppOutput.out(errorMsg);
      if (mainAppLogger.isLoggable(lvl)) {
        mainAppLogger.log(lvl, errorMsg);
      }
     */
  }

  public static void signal(Level lvl, boolean includingOutput, ErrorMessagesId error, Object... args) {
      final String errorMsg = String.format(ErrorMessages.get(error), args);
      signal(lvl, includingOutput, errorMsg);
  }

  public static void debugMessage(String debugMsg) {
    StackWalker stackWalker =
        StackWalker.getInstance(StackWalker.Option.RETAIN_CLASS_REFERENCE);
    StackWalker.StackFrame frame =
        stackWalker.walk(s ->
            s.skip(1).findFirst().orElse(null)
        );
    if (frame == null) {
      throw new Error("Error in ErrorMessages::debugMessage. See stack trace.");
    } else {
      signal(new Debug((Class<?>) frame.getDeclaringClass()), false, debugMsg);
    }
  }

  private static void logImplementationError(
      String specificError, boolean withStackTrace, int errorHandlingStackDepth
  ) {
    StackWalker stackWalker =
        StackWalker.getInstance(StackWalker.Option.RETAIN_CLASS_REFERENCE);
    StackWalker.StackFrame frame =
        stackWalker.walk(s ->
            s.skip(errorHandlingStackDepth).findFirst().orElse(null)
        );
    if (frame == null) {
      throw new Error("Error in ErrorMessages::logImplementationError. See stack trace.");
    } else {
      String errorMsg =
          String.format(
              ErrorMessages.get(ErrorMessagesId.IMPLEMENTATION_ERROR_IN_S_OF_S_AT_S_S),
              frame.getMethodName(), frame.getClassName(), frame.getLineNumber(),
              specificError
          );
      if (withStackTrace) {
        final StringBuilder errorSB = new StringBuilder(errorMsg);
        errorSB.append("\n  === Stack trace:");
        stackWalker.walk(s ->
            {
              s.skip(errorHandlingStackDepth)
                  .forEachOrdered(f ->
                      errorSB.append("\n  |- ").append(f.toStackTraceElement())
                  );
              return null;
            }
        );
        errorSB.append("\n  ================");
        errorMsg = errorSB.toString();
      }
      assert false : errorMsg;
      /*
      if (mainAppLogger.isLoggable(Level.SEVERE)) {
        mainAppLogger.severe(errorMsg);
      }
      */
    }
  }

  public static void logImplementationError(String specificError, boolean withStackTrace) {
      logImplementationError(specificError, withStackTrace, 2);
  }

  public static void logImplementationError(String specificError) {
      logImplementationError(specificError, LOG_IMPLEMENTATION_ERROR_WITH_STACKTRACE, 2);
  }

  /*
  public static void logSyntaxError(String fileName, ParserRuleContext ctx, String specificMsg) {
    ErrorMessages.signal(
        Level.WARNING, true, ErrorMessagesId.SYNTAX_ERROR_AT_L_S,
        fileName, ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine(),
        specificMsg
    );
  }
  */

}
