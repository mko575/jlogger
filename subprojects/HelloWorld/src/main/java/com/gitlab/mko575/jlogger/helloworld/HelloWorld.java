
package com.gitlab.mko575.jlogger.helloworld;

import com.gitlab.mko575.jlogger.ILogPattern;
import com.gitlab.mko575.jlogger.JLogger;
import com.gitlab.mko575.jlogger.subscribers.CLIErrorSubscriber;
import com.gitlab.mko575.jlogger.subscribers.CLIOutputSubscriber;
import com.gitlab.mko575.jlogger.subscribers.ILogSubscriber;
import com.gitlab.mko575.jlogger.subscribers.providers.defaultstderr.DefaultStdErrSubscriberProvider;
import com.gitlab.mko575.jlogger.tag.DebugLC;
import com.gitlab.mko575.jlogger.tag.ILogCategory;
import com.gitlab.mko575.jlogger.tag.ELogCategory;
import com.gitlab.mko575.jlogger.tag.LogTag;
import java.util.Optional;
import java.util.logging.Level;

public class HelloWorld {

    private enum MyLogMessages implements ILogPattern {
        TEST_TEMPLATE("This is a message template for %s."),
        ANOTHER_TEST(ELogCategory.USAGE_LOG, Level.WARNING, "This is another template.");

        private final ILogCategory dfltCategory;
        private final Level dfltLevel;
        private final String messageTemplate;

        MyLogMessages(ILogCategory cat, Level lvl, String template) {
            dfltCategory = cat;
            dfltLevel = lvl;
            messageTemplate = template;
        }

        MyLogMessages(String template) {
            this(null, null, template);
        }

        @Override
        public Optional<ILogCategory> getDefaultCategory() {
            return Optional.ofNullable(dfltCategory);
        }

        @Override
        public Optional<Level> getDefaultLevel() {
            return Optional.ofNullable(dfltLevel);
        }

        @Override
        public String getMessageTemplate() { return messageTemplate; }
    }

    public static void main(String[] args) {
        // I can add a subscriber of the same type as a default one already loaded ...
        JLogger.subscribe(
            new CLIOutputSubscriber("CLI output", null),
            LogTag.get(ELogCategory.USAGE_LOG, Level.SEVERE)
        );
        // I can change subscriptions for a default subscriber ...
        // Future implementations may require this to be done after calling JLogger.start()
        Optional<CLIErrorSubscriber> dfltErrSub =
            DefaultStdErrSubscriberProvider.provider().getSubscriber();
        dfltErrSub.ifPresent(cliErrorSubscriber ->
            JLogger.subscribe(
                cliErrorSubscriber,
                LogTag.get(ELogCategory.GENERIC_LOG, Level.ALL)
            )
        );

        JLogger.start();

        JLogger.log(LogTag.get(DebugLC.get(), Level.CONFIG), "This is a message with a log tag.");
        JLogger.log(DebugLC.get(), Level.INFO, "This is a message with a category and level.");
        JLogger.log(ELogCategory.IMPLEMENTATION_LOG, "This is a message with a category but no level.");
        JLogger.log(Level.WARNING, "This is a message with a level but no specific category.");
        JLogger.log("This is a message with no specific category nor level.");
        JLogger.log(LogTag.get(DebugLC.get(), Level.CONFIG), MyLogMessages.TEST_TEMPLATE, "a test");
        JLogger.log(DebugLC.get(), Level.INFO, MyLogMessages.TEST_TEMPLATE, "a test");
        JLogger.log(ELogCategory.IMPLEMENTATION_LOG, MyLogMessages.TEST_TEMPLATE, "a test");
        JLogger.log(Level.WARNING, MyLogMessages.TEST_TEMPLATE, "a test");
        JLogger.log(MyLogMessages.TEST_TEMPLATE, "a test");
        JLogger.log(MyLogMessages.ANOTHER_TEST);
        JLogger.debug("This is a 'debug' message without a level.");
        JLogger.debug(MyLogMessages.TEST_TEMPLATE, "Test");
        JLogger.debug(Level.SEVERE, "This is a 'debug' message with a specific level.");
        JLogger.trace();
        JLogger.trace(Level.SEVERE, "This is a 'trace' message.");
        JLogger.trace(DebugLC.get(), Level.INFO, "This is a 'trace' message.");
        JLogger.trace(LogTag.get(DebugLC.get(), Level.CONFIG), "This is a 'trace' message.");
        JLogger.trace(MyLogMessages.TEST_TEMPLATE, "Test");
        JLogger.implementationError("This is an 'implementation error' message without level.");
        JLogger.usageConfig("This is a 'usage config' message.");
        JLogger.usageInfo("This is a 'usage info' message.");
        JLogger.usageInfo(Level.FINEST, "This is a 'usage info' message with level.");
        JLogger.usageWarning("This is a 'usage warning' message.");
        JLogger.usageError("This is a 'usage error' message.");
        JLogger.log("END");

        JLogger.stop();
    }
}
