module jlogger.helloworld {
  requires java.logging; // Remove when ErrorMessages has been removed
  requires com.gitlab.mko.jlogger.core;
  requires com.gitlab.mko.jlogger.subscribers.providers.defaultstderr;
}
