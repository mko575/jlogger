import com.gitlab.mko575.jlogger.subscribers.ISubscriberProvider;
import com.gitlab.mko575.jlogger.subscribers.providers.jfxpopup.PopupLogSubscriberProvider;

module com.gitlab.mko.jlogger.subscribers.providers.jfxpopup {
  requires transitive com.gitlab.mko.jlogger.core;
  requires javafx.controls;
  requires javafx.swing;
  requires javafx.fxml;

  exports com.gitlab.mko575.jlogger.subscribers.providers.jfxpopup;

  provides ISubscriberProvider with PopupLogSubscriberProvider;
}
