package com.gitlab.mko575.jlogger.subscribers.providers.jfxpopup;

import com.gitlab.mko575.jlogger.JLogger;
import com.gitlab.mko575.jlogger.subscribers.ISubscriberProvider;
import java.util.concurrent.CountDownLatch;
import java.util.Optional;
import javafx.application.Platform;

/**
 * This utility class provides a default popup subscriber for logs handled by JLogger.
 * This subscriber is automatically loaded by {@link JLogger#start()} using
 * <code>ServiceLoader.load(ISubscriberProvider.class)</code> and the
 * {@link PopupLogSubscriberProvider#provider()} method.
 *
 * The provided subscriber has the name {@value #SUBSCRIBER_NAME}, defined in
 * {@link PopupLogSubscriberProvider#SUBSCRIBER_NAME}.
 * 
 * The implementation is based on <a href="https://openjfx.io">JavaFX</a>.
 *
 * @see ServiceLoader#load(Class)
 * @see ISubscriberProvider
 * @see <a href="https://openjfx.io">JavaFX</a>
 */
public final class PopupLogSubscriberProvider
    implements ISubscriberProvider<PopupLogSubscriber> {

  /**
   * Lone instance of this class.
   */
  private static final PopupLogSubscriberProvider LONE_INSTANCE =
      new PopupLogSubscriberProvider();

  /**
   * The subscriber provided by this provider.
   */
  private Optional<PopupLogSubscriber> subscriber;

  /**
   * Name of the subscriber provided: {@value #SUBSCRIBER_NAME}
   */
  public static final String SUBSCRIBER_NAME = "JLogger.JFXPopupSubscriber";

  /**
   * Private constructor preventing others to create new instances of this class.
   */
  private PopupLogSubscriberProvider() { super(); }

  /**
   * Returns the sole instance of this class.
   * @return the sole instance of this class.
   */
  public static PopupLogSubscriberProvider provider() {
    return LONE_INSTANCE;
  }

  @Override
  public Optional<PopupLogSubscriber> getSubscriber() {
    if (subscriber == null) {
      try {
        final CountDownLatch startupLatch = new CountDownLatch(1);
        Platform.startup(startupLatch::countDown);
        // Wait for the JavaFX Application Thread to be started
        try {
          startupLatch.await();
        } catch (InterruptedException e) {
          JLogger.implementationWarning(
              "Interrupted while waiting for the JavaFX Application Thread to be started."
                  + " I will try to continue anyway, but further exceptions may be triggered."
          );
        }
      } catch (IllegalStateException e) {
        JLogger.implementationInfo("JavaFx platform was already running.");
      }
      subscriber = Optional.of(new PopupLogSubscriber(SUBSCRIBER_NAME, null));
    }
    return subscriber;
  }
}
