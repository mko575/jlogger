import com.gitlab.mko575.jlogger.subscribers.ISubscriberProvider;
import com.gitlab.mko575.jlogger.subscribers.providers.defaultstdout.DefaultStdOutSubscriberProvider;

module com.gitlab.mko.jlogger.subscribers.providers.defaultstdout {
  requires transitive com.gitlab.mko.jlogger.core;

  exports com.gitlab.mko575.jlogger.subscribers.providers.defaultstdout;

  provides ISubscriberProvider with DefaultStdOutSubscriberProvider;
}
