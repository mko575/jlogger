package com.gitlab.mko575.jlogger.subscribers.providers.defaultstdout;

import com.gitlab.mko575.jlogger.JLogger;
import com.gitlab.mko575.jlogger.subscribers.CLIOutputSubscriber;
import com.gitlab.mko575.jlogger.subscribers.ISubscriberProvider;
import java.util.Optional;
import java.util.ServiceLoader;

/**
 * This utility class provides a default subscriber outputting on the standard output stream.
 * This subscriber is automatically loaded by {@link JLogger#start()} using
 * <code>ServiceLoader.load(ISubscriberProvider.class)</code> and the
 * {@link DefaultStdOutSubscriberProvider#provider()} method.
 *
 * The provided subscriber has the name {@value #SUBSCRIBER_NAME}, defined in
 * {@link DefaultStdOutSubscriberProvider#SUBSCRIBER_NAME}.
 *
 * @see ServiceLoader#load(Class)
 * @see ISubscriberProvider
 */
public final class DefaultStdOutSubscriberProvider
    implements ISubscriberProvider<CLIOutputSubscriber> {

  /**
   * Lone instance of this class.
   */
  private static final DefaultStdOutSubscriberProvider LONE_INSTANCE =
      new DefaultStdOutSubscriberProvider();

  /**
   * The subscriber provided by this instance.
   */
  private Optional<CLIOutputSubscriber> subscriber;

  /**
   * Name of the subscriber provided: {@value #SUBSCRIBER_NAME}
   */
  public static final String SUBSCRIBER_NAME = "JLogger.DefaultStdOutSubscriber";

  /**
   * Private constructor preventing others to create new instances of this class.
   */
  private DefaultStdOutSubscriberProvider() { super(); }

  /**
   * Returns the sole instance of this class.
   * @return the sole instance of this class.
   */
  public static DefaultStdOutSubscriberProvider provider() {
    return LONE_INSTANCE;
  }

  @Override
  public Optional<CLIOutputSubscriber> getSubscriber() {
    if (subscriber == null) {
      subscriber = Optional.of(
          new CLIOutputSubscriber(
              SUBSCRIBER_NAME,
              null)
      );
    }
    return subscriber;
  }
}
