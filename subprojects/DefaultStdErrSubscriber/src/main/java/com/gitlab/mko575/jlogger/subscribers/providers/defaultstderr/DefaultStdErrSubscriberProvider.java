package com.gitlab.mko575.jlogger.subscribers.providers.defaultstderr;

import com.gitlab.mko575.jlogger.JLogger;
import com.gitlab.mko575.jlogger.subscribers.CLIErrorSubscriber;
import com.gitlab.mko575.jlogger.subscribers.ISubscriberProvider;
import java.util.Optional;
import java.util.ServiceLoader;

/**
 * This utility class provides a default subscriber outputting on the standard error stream.
 * This subscriber is automatically loaded by {@link JLogger#start()} using
 * <code>ServiceLoader.load(ISubscriberProvider.class)</code> and the
 * {@link DefaultStdErrSubscriberProvider#provider()} method.
 *
 * The provided subscriber has the name {@value #SUBSCRIBER_NAME}, defined in
 * {@link DefaultStdErrSubscriberProvider#SUBSCRIBER_NAME}.
 *
 * @see ServiceLoader#load(Class)
 * @see ISubscriberProvider
 */
public final class DefaultStdErrSubscriberProvider
    implements ISubscriberProvider<CLIErrorSubscriber> {

  /**
   * Lone instance of this class.
   */
  private static final DefaultStdErrSubscriberProvider LONE_INSTANCE =
      new DefaultStdErrSubscriberProvider();

  /**
   * The subscriber provided by this instance.
   */
  private Optional<CLIErrorSubscriber> subscriber;

  /**
   * Name of the subscriber provided: {@value #SUBSCRIBER_NAME}
   */
  public static final String SUBSCRIBER_NAME = "JLogger.DefaultStdErrSubscriber";

  /**
   * Private constructor preventing others to create new instances of this class.
   */
  private DefaultStdErrSubscriberProvider() { super(); }

  /**
   * Returns the sole instance of this class.
   * @return the sole instance of this class.
   */
  public static DefaultStdErrSubscriberProvider provider() {
    return LONE_INSTANCE;
  }

  @Override
  public Optional<CLIErrorSubscriber> getSubscriber() {
    if (subscriber == null) {
      subscriber = Optional.of(
          new CLIErrorSubscriber(
              SUBSCRIBER_NAME,
              null)
      );
    }
    return subscriber;
  }
}
