import com.gitlab.mko575.jlogger.subscribers.ISubscriberProvider;
import com.gitlab.mko575.jlogger.subscribers.providers.defaultstderr.DefaultStdErrSubscriberProvider;

module com.gitlab.mko.jlogger.subscribers.providers.defaultstderr {
  requires transitive com.gitlab.mko.jlogger.core;

  exports com.gitlab.mko575.jlogger.subscribers.providers.defaultstderr;

  provides ISubscriberProvider with DefaultStdErrSubscriberProvider;
}
