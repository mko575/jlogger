module com.gitlab.mko.jlogger.core {
  requires transitive java.logging;

  exports com.gitlab.mko575.jlogger;
  exports com.gitlab.mko575.jlogger.tag;
  exports com.gitlab.mko575.jlogger.subscribers;
  exports com.gitlab.mko575.jlogger.subscribers.handlers;
  exports com.gitlab.mko575.jlogger.subscribers.formatters;

  uses com.gitlab.mko575.jlogger.subscribers.ISubscriberProvider;
}
