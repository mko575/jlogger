package com.gitlab.mko575.jlogger.subscribers.handlers;

import com.gitlab.mko575.jlogger.subscribers.formatters.LevelPrefixingFormatter;
import java.util.logging.LogRecord;
import java.util.logging.StreamHandler;

/**
 * Log record handler that outputs to <code>System.out</code>.
 */
public final class CLIOutputHandler extends StreamHandler {

  public static final long serialVersionUID = 835193369067L;

  /**
   * Constructor that returns a <code>StreamHandler</code> that outputs to
   * <code>System.out</code> using a {@link LevelPrefixingFormatter}.
   */
  public CLIOutputHandler() {
    super(System.out, new LevelPrefixingFormatter());
  }

  @Override
  public synchronized void publish(final LogRecord entry) { // NOPMD
    super.publish(entry);
    flush();
  }

  @Override
  public synchronized void close() { // NOPMD
    flush();
  }
}
