package com.gitlab.mko575.jlogger;

import com.gitlab.mko575.jlogger.subscribers.formatters.LogMessageProcessor;
import com.gitlab.mko575.jlogger.tag.LogTag;
import java.io.Serial;
import java.util.logging.LogRecord;

public class LogEntry extends LogRecord {

  @Serial
  private static final long serialVersionUID = 1L;

  private final LogTag tag;

  /**
   * Creates a log entry with the provided tag and message.
   * @param tag the tag characterizing the log entry's "topic".
   * @param message the complete message associated to this log entry.
   */
  public LogEntry(LogTag tag, String message) {
    super(tag.getLevel(), message);
    this.tag = tag;
  }

  /**
   * Creates a log entry with the provided tag and message.
   * @param tag the tag characterizing the log entry's "topic".
   * @param message the message associated to this log entry which is to be parameterized
   *               with the following argument.
   * @param parameters the values used to parameterize the message.
   */
  public LogEntry(LogTag tag, String message, Object[] parameters) {
    this(tag, message);
    if (parameters != null) {
      this.setParameters(parameters);
    }
  }

  public LogTag getTag() {
    return tag;
  }

  @Override
  public String toString() {
    return String.format(
        "[%s] %s: %s",
        getInstant().toString(),
        getTag(),
        LogMessageProcessor.process(this)
    );
  }
}
