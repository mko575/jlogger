package com.gitlab.mko575.jlogger.tag;

import com.gitlab.mko575.jlogger.tag.LogCategoryAsInterface.DebugLogCategoryAsClass;
import com.gitlab.mko575.jlogger.tag.LogCategoryAsInterface.ImplementationLogCategoryAsClass;
import com.gitlab.mko575.jlogger.tag.LogCategoryAsInterface.TraceLogCategoryAsClass;
import com.gitlab.mko575.jlogger.tag.LogCategoryAsInterface.UsageLogCategoryAsClass;

public enum ELogCategory implements ILogCategory  {
  GENERIC_LOG(LogCategoryAsInterface.class),
  IMPLEMENTATION_LOG(ImplementationLogCategoryAsClass.class),
  DEBUG_LOG(DebugLogCategoryAsClass.class),
  TRACE_LOG(TraceLogCategoryAsClass.class),
  USAGE_LOG(UsageLogCategoryAsClass.class);

  private final Class<? extends LogCategoryAsInterface> asClass;

  ELogCategory(Class<? extends LogCategoryAsInterface> c) {
    asClass = c;
  }

  @Override
  public Class<? extends LogCategoryAsInterface> asClass() {
    return asClass;
  }
}
