package com.gitlab.mko575.jlogger.subscribers;

import com.gitlab.mko575.jlogger.JLogger;
import com.gitlab.mko575.jlogger.LogEntry;
import com.gitlab.mko575.jlogger.subscribers.formatters.ToStringFormatter;
import com.gitlab.mko575.jlogger.tag.ELogCategory;
import com.gitlab.mko575.jlogger.tag.LogTag;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FileSubscriber extends Logger implements ILogSubscriber {

  public FileSubscriber(String name, String resourceBundleName) throws IOException {
    super(name, resourceBundleName);
    final String logFileName =
        JLogger.getProperties().getProperty(
            "jlogger.dfltFileSubscriber.logFileName", "%t/JLogger/run-%u_%g.log"
        );
    final int fileSizeLimit =
        Integer.parseInt(
          JLogger.getProperties().getProperty(
              "jlogger.dfltFileSubscriber.fileSizeLimit", "1000000"
          )
        );
    final int nbFilesLimit =
        Integer.parseInt(
          JLogger.getProperties().getProperty(
              "jlogger.dfltFileSubscriber.nbFilesLimit", "2"
          )
        );
    final boolean append =
        Boolean.parseBoolean(
          JLogger.getProperties().getProperty(
              "jlogger.dfltFileSubscriber.append", "false"
          )
        );
    FileHandler msgHandler = null;
    try {
      msgHandler = new FileHandler(logFileName, fileSizeLimit, nbFilesLimit, append);
    } catch (NoSuchFileException e) {
      Files.createDirectories(Path.of(e.getFile()).getParent());
      msgHandler = new FileHandler(logFileName, fileSizeLimit, nbFilesLimit, append);
    }
    msgHandler.setFormatter(new ToStringFormatter());
    this.addHandler(msgHandler);
  }

  @Override
  public LogTag getDefaultSubscriptionTopic() {
    return LogTag.get(ELogCategory.GENERIC_LOG, Level.ALL);
  }

  @Override
  public void onNext(LogEntry log) {
    this.log(log);
  }
}
