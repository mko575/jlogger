package com.gitlab.mko575.jlogger;

import com.gitlab.mko575.jlogger.internals.LogBroker;
import com.gitlab.mko575.jlogger.subscribers.ILogSubscriber;
import com.gitlab.mko575.jlogger.subscribers.ISubscriberProvider;
import com.gitlab.mko575.jlogger.tag.DebugLC;
import com.gitlab.mko575.jlogger.tag.ELogCategory;
import com.gitlab.mko575.jlogger.tag.ILogCategory;
import com.gitlab.mko575.jlogger.tag.LogTag;
import java.io.IOException;
import java.io.InputStream;
import java.lang.StackWalker.StackFrame;
import java.util.Arrays;
import java.util.Optional;
import java.util.Properties;
import java.util.ServiceLoader;
import java.util.logging.Level;

/**
 * Class presenting the main JLogger API.
 */
public class JLogger {

  private static final Properties properties = new Properties();

  /**
   * Default log level to use if none is provided or found.
   */
  private static Level dfltLogLevel = Level.ALL;

  {
    // Load properties from configuration file
    ClassLoader loader = Thread.currentThread().getContextClassLoader();
    InputStream stream = loader.getResourceAsStream("/jlogger.properties");
    try {
      properties.load(stream);
    } catch (IOException e) {
      e.printStackTrace();
    }

    // Configure from properties
    dfltLogLevel = Level.parse(properties.getProperty("jlogger.dfltLogLevel", "ALL"));
  }

  /**
   * Keeps track of howmuch entities asked for logging startup.
   */
  private static boolean subscribersLoaded = false;

  /**
   * Keeps track of howmuch entities asked for logging startup.
   */
  private static int nbStartups = 0;

  /**
   * Private constructor throwing an exception to discourage instantiating or
   * inheriting from this class.
   *
   * @throws AssertionError always
   */
  private JLogger() throws AssertionError {
    throw new AssertionError(
        "JLogger is a utility class."
            + " No instance of it should ever be created!");
  }

  /**
   * Starts the logging framework. No logging before calling this method.
   * Can be called multiple times.
   */
  public static void start() {
    System.out.printf(
        "Starting JLogger while nbStartups=%d!%n",
        nbStartups
    );
    if (! subscribersLoaded) {
      subscribersLoaded = true;
      ServiceLoader.load(ISubscriberProvider.class).forEach(
          provider -> {
            @SuppressWarnings("unchecked")
            ISubscriberProvider<ILogSubscriber> sp = provider;
            Optional<ILogSubscriber> sOpt = sp.getSubscriber();
            sOpt.ifPresent(
                s -> LogBroker.subscribe(s, sp.getTopic())
            );
          }
      );
    }
    if (nbStartups == 0) { LogBroker.start(); }
    nbStartups += 1;
  }

  /**
   * Returns the JLogger specific properties.
   * @return the JLogger specific properties.
   */
  public static Properties getProperties() {
    return properties;
  }

  /**
   * Register the provided log subscriber to receive logs covered by its default topic.
   * @param subscriber the log subscriber to register.
   */
  public static void subscribe(ILogSubscriber subscriber) {

    LogBroker.subscribe(subscriber, Optional.empty());
  }

  /**
   * Register the provided log subscriber to receive logs covered by its default topic.
   * @param subscriber the log subscriber to register.
   */
  public static void subscribe(ILogSubscriber subscriber, LogTag topic) {
    LogBroker.subscribe(subscriber, Optional.of(topic));
  }

  /**
   * Ask to stops the logging mechanism and free all its resources. As an effect
   * only if all who have started it, have also stopped it; or if stoppping is forced.
   * @param force force stopping logging.
   */
  public static void stop(boolean force) {
    System.out.printf(
        "Stopping JLogger while nbStartups=%d (forcing is %s)!%n",
        nbStartups, force
    );
    nbStartups = nbStartups > 0 ? nbStartups - 1 : 0; // in case counting is out of sync
    if (nbStartups == 0 || force) {
      LogBroker.stop();
    }
  }

  /**
   * Ask to stops the logging mechanism and free all its resources. As an effect
   * only if all who have started it, have also stopped it.
   */
  public static void stop() {
    stop(false);
  }

  /**
   * Internal method to log an entry.
   * @param entry the entry to log.
   */
  protected static void logEntry(LogEntry entry) {
    LogBroker.dispatch(entry);
  }

  /* HELPERS */

  /**
   * Internal class to handle API logging methods arguments.
   */
  protected static final class LogArguments {
    private final LogTag tag;
    private final String msg;
    private final Object[] parameters;

    /**
     * Internal constructor
     * @param tag the tag of the log entry.
     * @param msg the raw message of the log entry.
     * @param params the values to parameterize the raw message.
     */
    private LogArguments(LogTag tag, String msg, Object[] params) {
      this.tag = tag;
      this.msg = msg;
      this.parameters = params;
    }

    protected LogTag getTag() {
      return tag;
    }

    protected String getMessage() {
      return msg;
    }

    protected Object[] getParameters() { return parameters; }

    protected LogEntry toLogEntry() {
      return new LogEntry(getTag(), getMessage(), getParameters());
    }

    /**
     * Static method to parse API logging methods arguments.
     * @param args the arguments to parse.
     * @return a LogArguments object representing the parsed arguments.
     */
    protected static LogArguments parse(Object... args) {
      int nextArgument = 0;
      ILogCategory cat = ELogCategory.GENERIC_LOG;
      Optional<Level> oLvl = Optional.empty();
      String msg;
      Object[] params = null;
      while (
          args.length > nextArgument && (
              args[nextArgument] instanceof LogTag
              || args[nextArgument] instanceof ILogCategory
              || args[nextArgument] instanceof Level
          )
      ) {
        if (args[nextArgument] instanceof LogTag) {
          oLvl = Optional.of(((LogTag) args[nextArgument]).getLevel());
        } else if (args[nextArgument] instanceof ILogCategory) {
          cat = ((ILogCategory) args[nextArgument]);
        } else if (args[nextArgument] instanceof Level) {
          oLvl = Optional.of(((Level) args[nextArgument]));
        }
        nextArgument += 1;
      }
      if (args.length > nextArgument && args[nextArgument] instanceof ILogPattern) {
        ILogPattern logMsg = ((ILogPattern) args[nextArgument]);
        nextArgument += 1;
        oLvl = oLvl.or(logMsg::getDefaultLevel);
        msg = logMsg.getMessageTemplate();
        if (nextArgument < args.length)
          params = Arrays.copyOfRange(args, nextArgument, args.length);
      } else {
        if (nextArgument < args.length && args[nextArgument] instanceof String) {
          msg = ((String) args[nextArgument]);
          nextArgument += 1;
          if (nextArgument < args.length)
            params = Arrays.copyOfRange(args, nextArgument, args.length);
        } else {
          msg = "";
        }
      }
      return new LogArguments(LogTag.get(cat, oLvl.orElse(dfltLogLevel)), msg, params);
    }
  }

  /**
   * Helper method to retrieve the stack frame of the method requesting the logging.
   * @param initiatorDepth the depth of the initial requester in the call stack.
   * @return the stack frame of the initial requester.
   */
  private static StackFrame getStackFrame(int initiatorDepth) {
    StackWalker stackWalker =
        StackWalker.getInstance(StackWalker.Option.RETAIN_CLASS_REFERENCE);
    StackWalker.StackFrame frame =
        stackWalker.walk(s ->
            s.skip(initiatorDepth + 1).findFirst().orElse(null)
        );
    if (frame == null) {
      throw new Error("Error in trace logging method. See stack trace.");
    }
    return frame;
  }

  /* DEFAULT LOGGING METHOD */

  private static void log(Object... args) {
    log(ELogCategory.GENERIC_LOG, Level.FINE, args);
  }

  private static void log(ILogCategory cat, Object... args) {
    log(cat, Level.FINE, args);
  }

  private static void log(ILogCategory cat, Level lvl, Object... args) {
    Object[] updatedArgs = new Object[args.length + 2];
    updatedArgs[0] = cat;
    updatedArgs[1] = lvl;
    System.arraycopy(args, 0, updatedArgs, 2, args.length);
    LogArguments logArgs = LogArguments.parse(updatedArgs);
    logEntry(logArgs.toLogEntry());
  }

  /**
   * Default logging method.
   * @param tag tag used to filter which log subscribers receive the message.
   * @param msg message to log.
   */
  public static void log(LogTag tag, String msg) {
    logEntry(new LogEntry(tag, msg));
  }

  /* PARAMETERS VARIANTS OF DEFAULT LOGGING METHOD */

  public static void log(ILogCategory cat, Level lvl, String msg) {
    log(LogTag.get(cat, lvl), msg);
  }

  public static void log(ILogCategory cat, String msg) {
    log(cat, dfltLogLevel, msg);
  }
  
  public static void log(Level lvl, String msg) {
    log(ELogCategory.GENERIC_LOG, lvl, msg);
  }

  public static void log(String msg) {
    log(ELogCategory.GENERIC_LOG, dfltLogLevel, msg);
  }

  public static void log(LogTag tag, ILogPattern msg, Object... args) {
    log(tag, String.format(msg.getMessageTemplate(), args));
  }

  public static void log(ILogCategory cat, Level lvl, ILogPattern msg, Object... args) {
    log(cat, lvl, String.format(msg.getMessageTemplate(), args));
  }

  public static void log(ILogCategory cat, ILogPattern msg, Object... args) {
    log(cat, String.format(msg.getMessageTemplate(), args));
  }

  public static void log(Level lvl, ILogPattern msg, Object... args) {
    log(lvl, String.format(msg.getMessageTemplate(), args));
  }

  public static void log(ILogPattern msg, Object... args) {
    log(String.format(msg.getMessageTemplate(), args));
  }

  /* CATEGORY SPECIFIC METHODS */

  public static void debug(Object... args) {
    LogArguments logArgs = LogArguments.parse(DebugLC.get(1), args);
    logEntry(new LogEntry(logArgs.getTag(), logArgs.getMessage()));
  }

  public static void trace(Object... args) {
    LogArguments logArgs = LogArguments.parse(ELogCategory.TRACE_LOG, args);
    StackFrame frame = getStackFrame(1);
    logEntry(new LogEntryWithStackFrame(logArgs.getTag(), frame, logArgs.getMessage()));
  }

  /* IMPLEMENTATION LOGS */

  /*
  public static void implementationLog(Object... args) {
    LogArguments logArgs = LogArguments.parse(ELogCategory.IMPLEMENTATION_LOG, args);
    logEntry(new LogEntry(logArgs.getTag(), logArgs.getMessage()));
  }
   */

  public static void implementationLog(Level lvl, Object... args) {
    log(ELogCategory.IMPLEMENTATION_LOG, lvl, args);
  }

  public static void implementationConfig(Object... args) {
    log(ELogCategory.IMPLEMENTATION_LOG, Level.CONFIG, args);
  }

  public static void implementationInfo(Object... args) {
    log(ELogCategory.IMPLEMENTATION_LOG, Level.INFO, args);
  }

  public static void implementationWarning(Object... args) {
    log(ELogCategory.IMPLEMENTATION_LOG, Level.WARNING, args);
  }

  public static void implementationError(Object... args) {
    log(ELogCategory.IMPLEMENTATION_LOG, Level.SEVERE, args);
  }

  /* USAGE LOGS */

  /*
  public static void usageLog(Object... args) {
    LogArguments logArgs = LogArguments.parse(ELogCategory.USAGE_LOG, args);
    logEntry(logArgs.toLogEntry());
  }
  */

  public static void usageLog(Level lvl, Object... args) {
    log(ELogCategory.USAGE_LOG, lvl, args);
  }

  public static void usageConfig(Object... args) {
    log(ELogCategory.USAGE_LOG, Level.CONFIG, args);
  }

  public static void usageInfo(Object... args) {
    log(ELogCategory.USAGE_LOG, Level.INFO, args);
  }

  public static void usageWarning(Object... args) {
    log(ELogCategory.USAGE_LOG, Level.WARNING, args);
  }

  public static void usageError(Object... args) {
    log(ELogCategory.USAGE_LOG, Level.SEVERE, args);
  }

}
