package com.gitlab.mko575.jlogger.tag;

import java.io.Serial;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Category for debug messages.
 * @param <T> The class generating the debug message.
 */
public class DebugLC<T> implements ILogCategory {

  @Serial
  private static final long serialVersionUID = 1L;


  private static final Map<Class<?>, DebugLC<?>> INSTANCES = new HashMap<>();

  /**
   * The class generating the debug messages of this category.
   */
  private final Class<T> debuggedClass;

  /**
   * Basic constructor
   * @param c the class generating the debug messages of this category.
   */
  private DebugLC(Class<T> c) {
    debuggedClass = c;
  }

  /**
   * Basic constructor
   * @param c the class generating the debug messages of this category.
   * @return
   */
  public static <U> DebugLC<U> get(Class<U> c) {
    DebugLC<U> cat = null;
    if (INSTANCES.containsKey(c)) {
      @SuppressWarnings("unchecked")
      DebugLC<U> catCast = (DebugLC<U>) INSTANCES.get(c);
      cat = catCast;
    } else {
      cat = new DebugLC<>(c);
      INSTANCES.put(c, cat);
    }
    return cat;
  }

  /**
   * Constructor retrieving the debugged class from the call stack.
   * @param initiatorDepth depth of the caller debugged (number of calls since the debugged "spot").
   */
  public static DebugLC<?> get(int initiatorDepth) {
    StackWalker stackWalker =
        StackWalker.getInstance(StackWalker.Option.RETAIN_CLASS_REFERENCE);
    StackWalker.StackFrame frame =
        stackWalker.walk(s ->
            s.skip(initiatorDepth + 1).findFirst().orElse(null)
        );
    if (frame == null) {
      throw new RuntimeException("Error in Debug constructor. See stack trace.");
    }
    Class<?> debuggedClass = frame.getDeclaringClass();
    return get(debuggedClass);
  }

  /**
   * Retrieve the debug category for the caller.
   * @return the debug category for the caller.
   */
  public static DebugLC<?> get() {
    return get(1);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null) {
      return false;
    }
    if (o instanceof DebugLC) {
      return Objects.equals(debuggedClass, ((DebugLC<?>) o).debuggedClass);
    } else {
      return false;
    }
  }

  @Override
  public int hashCode() {
    return Objects.hash(DebugLC.class, debuggedClass);
  }

  @Override
  public Class<? extends LogCategoryAsInterface> asClass() {
    return ELogCategory.DEBUG_LOG.asClass();
  }

  @Override
  public boolean includedIn(ILogCategory cat) {
    if (cat instanceof DebugLC) {
      return ((DebugLC<?>) cat).debuggedClass.isAssignableFrom(debuggedClass);
    } else {
      return ILogCategory.super.includedIn(cat);
    }
  }

  @Override
  public String toString() {
    return String.format("DEBUG<%s>", debuggedClass.getCanonicalName());
  }

}
