package com.gitlab.mko575.jlogger.subscribers;

import com.gitlab.mko575.jlogger.LogEntry;
import com.gitlab.mko575.jlogger.tag.ELogCategory;
import com.gitlab.mko575.jlogger.tag.LogTag;
import java.util.logging.Level;

public interface ILogSubscriber {

  default LogTag getDefaultSubscriptionTopic() {
    return LogTag.get(ELogCategory.GENERIC_LOG, Level.ALL);
  }

  void onNext(LogEntry log);

}
