package com.gitlab.mko575.jlogger.subscribers.formatters;

import java.util.logging.Formatter;
import java.util.logging.LogRecord;

/**
 * Log record formatter that simply uses the ToString method of the record.
 */
public final class ToStringFormatter extends Formatter {

  /**
   * Default constructor simply calling "super"
   */
  public ToStringFormatter() { super(); }

  @Override
  public String format(final LogRecord record) {
    return String.format("%s%n", record.toString());
  }
}
