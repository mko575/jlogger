package com.gitlab.mko575.jlogger;

import com.gitlab.mko575.jlogger.subscribers.formatters.LogMessageProcessor;
import com.gitlab.mko575.jlogger.tag.LogTag;
import java.io.Serial;
import java.lang.StackWalker.StackFrame;

public class LogEntryWithStackFrame extends LogEntry {

  @Serial
  private static final long serialVersionUID = 1L;

  private final StackWalker.StackFrame frame;

  public LogEntryWithStackFrame(LogTag tag, StackFrame frame, String message) {
    super(tag, message);
    this.frame = frame;
  }

  @Override
  public String toString() {
    return String.format(
        "[%s] %s At %s#%s#%d: %s",
        getInstant().toString(), getTag(),
        frame.getClassName(), frame.getMethodName(), frame.getLineNumber(),
        LogMessageProcessor.process(this)
    );
  }
}
