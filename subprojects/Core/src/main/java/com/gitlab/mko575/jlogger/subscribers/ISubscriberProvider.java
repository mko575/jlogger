package com.gitlab.mko575.jlogger.subscribers;

import com.gitlab.mko575.jlogger.tag.LogTag;
import java.util.Optional;

/**
 * Interface for subscribers automatically registered at load time.
 */
public interface ISubscriberProvider<T extends ILogSubscriber> {

  /**
   * Returns the {@link ILogSubscriber} to register automatically at load time.
   * Every call to this method should always return the same object.
   * @return
   */
  Optional<T> getSubscriber();

  /**
   * Returns the topic to use to register the subscriber provided.
   * @return the topic to register the subscriber with.
   */
  default Optional<LogTag> getTopic() {
    return getSubscriber().map(ILogSubscriber::getDefaultSubscriptionTopic);
  }

}
