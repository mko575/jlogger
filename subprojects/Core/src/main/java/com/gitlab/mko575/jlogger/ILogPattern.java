package com.gitlab.mko575.jlogger;

import com.gitlab.mko575.jlogger.tag.ILogCategory;
import java.util.Optional;
import java.util.logging.Level;

/**
 * Interface for generic log messages.
 */
public interface ILogPattern {

  /**
   * Returns the default category to be used for this generic log message.
   * @return the default category.
   */
  Optional<ILogCategory> getDefaultCategory();

  /**
   * Returns the default level to be used for this generic log message.
   * @return the default level.
   */
  Optional<Level> getDefaultLevel();

  /**
   * Returns the template to be used to build the message associated to this generic log message.
   * @return the template to use.
   */
  String getMessageTemplate();
}
