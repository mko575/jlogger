package com.gitlab.mko575.jlogger.subscribers;

import com.gitlab.mko575.jlogger.LogEntry;
import com.gitlab.mko575.jlogger.subscribers.formatters.ToStringFormatter;
import com.gitlab.mko575.jlogger.tag.ELogCategory;
import com.gitlab.mko575.jlogger.tag.LogTag;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CLIErrorSubscriber extends Logger implements ILogSubscriber {

  public CLIErrorSubscriber(String name, String resourceBundleName) {
    super(name, resourceBundleName);
    final Handler msgHandler = new ConsoleHandler();
    msgHandler.setFormatter(new ToStringFormatter());
    msgHandler.setLevel(Level.ALL);
    this.addHandler(msgHandler);
    this.setLevel(Level.ALL);
  }

  @Override
  public LogTag getDefaultSubscriptionTopic() {
    return LogTag.get(ELogCategory.IMPLEMENTATION_LOG, Level.SEVERE);
  }

  @Override
  public void onNext(LogEntry log) {
    this.log(log);
  }
}
