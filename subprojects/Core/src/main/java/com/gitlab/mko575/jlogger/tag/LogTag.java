package com.gitlab.mko575.jlogger.tag;

import java.io.Serial;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Level;
// import java.lang.System.Logger.Level;

/**
 * Class describing log tags used to filter log messages to "display" or not.
 */
public class LogTag implements Serializable {

  @Serial
  private static final long serialVersionUID = 1L;

  private static final Map<LogTag, LogTag> INSTANCES = new HashMap<>();

  /**
   * The category associated to the log message tagged.
   */
  private final ILogCategory category;

  /**
   * The "importance" level associated to the log message tagged.
   */
  private final Level level;

  /**
   * Constructor of log tags.
   * @param cat the category associated to the tag.
   * @param lvl the "importance" level associated to the tag.
   */
  private LogTag(ILogCategory cat, Level lvl) {
    category = Objects.requireNonNull(cat);
    level = Objects.requireNonNull(lvl);
  }

  /**
   * Retrieves the log tag associated to the provided category and level.
   * @param cat the category associated to the tag.
   * @param lvl the "importance" level associated to the tag.
   * @return the associated log tag.
   */
  public static LogTag get(ILogCategory cat, Level lvl) {
    LogTag tag = new LogTag(cat, lvl);
    if (INSTANCES.containsKey(tag)) {
      return INSTANCES.get(tag);
    } else {
      INSTANCES.put(tag,tag);
      return tag;
    }
  }

  /**
   * Retrieves the category associated to this tag.
   * @return the associated category.
   */
  public ILogCategory getCategory() {
    return category;
  }

  /**
   * Retrieves the level associated to this tag.
   * @return the associated level.
   */
  public Level getLevel() {
    return level;
  }

  /**
   * Test if this tag is included in the otherTag (i.e. if logs with this tag should be sent to
   * logger of the otherTag).
   * @param otherTag the expected including tag.
   * @return true if this tag is included (covered) by the otherTag.
   */
  public boolean includedIn(LogTag otherTag) {
    return
        getCategory().includedIn(otherTag.getCategory())
        && getLevel().intValue() >= otherTag.getLevel().intValue();
  }

  @Override
  public String toString() {
    return String.format("(%s, %s)", category, level);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    return Objects.equals(category, ((LogTag) o).category)
        && Objects.equals(level, ((LogTag) o).level);
  }

  @Override
  public int hashCode() {
    return Objects.hash(getClass(), category.hashCode(), level.hashCode());
  }
}
