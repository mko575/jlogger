package com.gitlab.mko575.jlogger.tag;

public interface LogCategoryAsInterface {

  interface ImplementationLogCategoryAsClass extends LogCategoryAsInterface {}

  interface DebugLogCategoryAsClass extends ImplementationLogCategoryAsClass {}
  interface TraceLogCategoryAsClass extends ImplementationLogCategoryAsClass {}

  interface UsageLogCategoryAsClass extends LogCategoryAsInterface {}

}
