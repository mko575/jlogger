package com.gitlab.mko575.jlogger.internals;

import com.gitlab.mko575.jlogger.JLogger;
import com.gitlab.mko575.jlogger.LogEntry;
import com.gitlab.mko575.jlogger.subscribers.ILogSubscriber;
import com.gitlab.mko575.jlogger.tag.LogTag;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Static class handling log dispatching.
 */
// @SuppressWarnings("java:S1118")
public class LogBroker {

  private static boolean active = false;

  /**
   * Mapping from log tags to subscribers that should get messages with this tag.
   */
  private static final Map<LogTag, Set<ILogSubscriber>> subscriptions = new HashMap<>();

  /**
   * Mapping from subscribers the executor service handling them.
   */
  private static final Map<ILogSubscriber, ExecutorService> executors = new HashMap<>();

  /*
  private static final ThreadPoolExecutor executor =
      (ThreadPoolExecutor) Executors.newFixedThreadPool(
          Integer.parseInt(JLogger.getProperties().getProperty("jlogger.nbDispatchThreads", "4"))
      );
   */

  /**
   * Private constructor for this 'singleton' class.
   */
  private LogBroker() {
    throw new UnsupportedOperationException("LogBroker constructor should never be called!");
  }

  /**
   * Starts the log broker. No log messages are dispatch unless the log broker is started.
   */
  public static synchronized void start() {
    active = true;
    System.out.println("Starting LogBroker!");
    // Making sure all executors of registered subscribers have been shutdown.
    executors.values().forEach(ExecutorService::shutdown);
    // Starting new executor for every registered subscriber
    executors.keySet().forEach(k -> executors.put(k, Executors.newSingleThreadExecutor()));
  }

  public static void subscribe(final ILogSubscriber subscriber, final Optional<LogTag> topic) {
    addSubscription(subscriber, topic.orElse(subscriber.getDefaultSubscriptionTopic()));
  }

  public static synchronized void addSubscription(
      final ILogSubscriber subscriber,
      final LogTag subscriptionTopic
  ) {
    if ( ! subscriptions.containsKey(subscriptionTopic) ) {
      initSubscriptionEntryFor(subscriptionTopic);
    }
    executors.computeIfAbsent(
        subscriber,
        k -> Executors.newSingleThreadExecutor()
    );
    subscriptions.forEach(
        (topic, subscribers) -> {
          if ( topic.includedIn(subscriptionTopic) ) subscribers.add(subscriber);
        }

    );
  }

  public static synchronized void removeSubscription(
      final ILogSubscriber subscriber,
      final LogTag subscriptionTopic
  ) {
    subscriptions.forEach(
        (topic, subscribers) -> {
          if ( subscriptionTopic.includedIn(topic) ) subscribers.remove(subscriber);
        }
    );
  }

  private static synchronized void initSubscriptionEntryFor(final LogTag tag) {
    final Set<ILogSubscriber> subscriberSet = new HashSet<>();
    subscriptions.forEach(
        (t, s) -> {
          if ( tag.includedIn(t) ) subscriberSet.addAll(s);
        }
    );
    subscriptions.put(tag, subscriberSet);
  }

  /**
   * Dispatch the log entry to all concerned log subscribers.
   * @param logEntry the log entry to dispatch.
   */
  public static synchronized void dispatch(final LogEntry logEntry) {
    LogTag tag = logEntry.getTag();
    if ( ! subscriptions.containsKey(tag) ) {
      initSubscriptionEntryFor(tag);
    }
    if ( active )
      subscriptions.get(tag).forEach(
          s -> executors.get(s).execute(() -> s.onNext(logEntry))
      );
  }

  /**
   * Stops the log broker. No further log entries will be dispatch until the log broker is
   * restarted.
   */
  public static synchronized void stop() {
    System.out.println("Stopping the LogBroker!");
    active = false;
    executors.values().forEach(ExecutorService::shutdown);
  }

  /**
   * Shutdowns the log broken "forcefully".
   */
  public static synchronized void shutdown() {
    active = false;
    executors.values().forEach(ExecutorService::shutdownNow);
  }

}
