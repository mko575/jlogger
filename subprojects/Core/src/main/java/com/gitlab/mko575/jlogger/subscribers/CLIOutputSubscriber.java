package com.gitlab.mko575.jlogger.subscribers;

import com.gitlab.mko575.jlogger.LogEntry;
import com.gitlab.mko575.jlogger.subscribers.handlers.CLIOutputHandler;
import com.gitlab.mko575.jlogger.tag.ELogCategory;
import com.gitlab.mko575.jlogger.tag.LogTag;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.StreamHandler;

public class CLIOutputSubscriber extends Logger implements ILogSubscriber {

  public static final long serialVersionUID = 283712505610L;

  public CLIOutputSubscriber(String name, String resourceBundleName) {
    super(name, resourceBundleName);
    final StreamHandler mainOutputHandler = new CLIOutputHandler();
    mainOutputHandler.setLevel(Level.ALL);
    this.addHandler(mainOutputHandler);
    this.setLevel(Level.ALL);
  }

  @Override
  public LogTag getDefaultSubscriptionTopic() {
    return LogTag.get(ELogCategory.USAGE_LOG, Level.WARNING);
  }

  @Override
  public void onNext(LogEntry log) {
    this.log(log);
  }
}
