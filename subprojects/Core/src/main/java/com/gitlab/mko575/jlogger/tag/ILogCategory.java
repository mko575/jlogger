package com.gitlab.mko575.jlogger.tag;

import java.io.Serializable;

/**
 * Interface of categories used in log message tags.
 */
public interface ILogCategory extends Serializable {

  Class<? extends LogCategoryAsInterface> asClass();

  default boolean includedIn(ILogCategory cat) {
    return cat.asClass().isAssignableFrom(this.asClass());
  }

}
