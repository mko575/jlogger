package com.gitlab.mko575.jlogger.subscribers.formatters;

import java.util.ResourceBundle;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;

/**
 * Log record formatter that prefixes the message with its colored level.
 */
public final class LevelPrefixingFormatter extends Formatter {

  private static final String RESET_TEXT = "\033[0m";
  private static final String SEVERE_TEXT = "\033[1;31m";
  private static final String WARNING_TEXT = "\033[1;33m";
  private static final String INFO_TEXT = "\033[1;32m";

  /**
   * Default constructor simply calling "super"
   */
  public LevelPrefixingFormatter() { super(); }

  @Override
  public String format(final LogRecord record) {
    String textColor = "";
    Level lvl = record.getLevel();
    if (lvl.equals(Level.SEVERE)) {
      textColor = SEVERE_TEXT;
    } else if (lvl.equals(Level.WARNING)) {
      textColor = WARNING_TEXT;
    } else if (lvl.equals(Level.INFO)) {
      textColor = INFO_TEXT;
    }
    return
        String.format("[%s%-7s%s] %s%n",
            textColor,
            lvl.getName(),
            RESET_TEXT,
            LogMessageProcessor.process(record)
        );
  }
}
