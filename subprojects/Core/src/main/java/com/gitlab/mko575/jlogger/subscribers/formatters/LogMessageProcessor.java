package com.gitlab.mko575.jlogger.subscribers.formatters;

import java.util.ResourceBundle;
import java.util.logging.LogRecord;

public class LogMessageProcessor {

  /**
   * Private constructor for this 'helper' class.
   */
  private LogMessageProcessor() {
    throw new UnsupportedOperationException("LogMessageProcessor constructor should never be called!");
  }

  public static String process(LogRecord logRecord) {
    final String rawMsg = logRecord.getMessage();
    final Object[] params = logRecord.getParameters();
    final ResourceBundle bundle = logRecord.getResourceBundle();
    final String localizedMsg;
    final String processedMsg;
    if ( bundle != null && rawMsg != null && bundle.containsKey(rawMsg) ) {
      localizedMsg = bundle.getString(rawMsg);
    } else {
      localizedMsg = rawMsg;
    }
    if ( logRecord.getParameters() != null ) {
      if (bundle == null) {
        processedMsg = String.format(localizedMsg, params);
      } else {
        processedMsg = String.format(bundle.getLocale(), localizedMsg, params);
      }
    } else {
      processedMsg = rawMsg;
    }
    return processedMsg;
  }

}
